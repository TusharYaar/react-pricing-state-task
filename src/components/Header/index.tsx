import React from "react";

import "./styles.css";
const Header = ({ children, title }: { children: React.ReactNode; title: string }) => {
  return (
    <div className="header_container">
      <h1 className="title">{title}</h1>
      <p className="description">{children}</p>
    </div>
  );
};

export default Header;
