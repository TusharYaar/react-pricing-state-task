import "./styles.css";

interface Props {
  title: string;
  links: {
    text: string;
    href: string;
  }[];
}

const Footer = ({ title, links }: Props) => {
  return (
    <div className="footer-section">
      <h4 className="footer-section-title">{title}</h4>
      <ul className="footer-section-list">
        {links.map((link, index) => {
          return (
            <li key={index}>
              <a href={link.href}>{link.text}</a>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default Footer;
