import "./styles.css";

import StarImg from "../../star.png";

import Button from "../Button";
export interface Props {
  title: string;
  price: number;
  features: string[];
  buttonText: string;
  buttonVariant: "outlined" | "contained";
  isStarted: boolean;
}

interface PropsExtended extends Props {
  isSelected: boolean;
  updatePlan: (title: string) => void;
}

const PriceCard = ({
  title,
  price,
  features,
  buttonText,
  buttonVariant,
  isStarted,
  isSelected,
  updatePlan,
}: PropsExtended) => {
  const handleSelect = () => {
    updatePlan(title);
  };

  return (
    <div className={`price-card ${isSelected ? "active" : ""}`}>
      <div className="price-card-header">
        {isStarted && <img className="price-card-header-star" src={StarImg} alt="star" />}
        <h3 className="price-card-title">{title}</h3>

        {isSelected && <p className="price-card-selected">Selected</p>}
      </div>
      <div className="price-card-body">
        <h1 className="price-card-price">
          ${price}
          <span className="price-card-mo">/mo</span>
        </h1>
        <ul className="price-card-features">
          {features.map((feature, index) => (
            <li key={index}>{feature}</li>
          ))}
        </ul>
        <Button
          title={isSelected ? "Selected" : buttonText}
          type={isSelected ? "contained" : "outlined"}
          onClick={handleSelect}
        />
      </div>
    </div>
  );
};

export default PriceCard;
