import React from "react";
import "./styles.css";
const Button = ({
  title,
  type,
  onClick,
}: {
  title: string;
  type: "outlined" | "contained";
  onClick: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}) => {
  return (
    <button onClick={(e) => onClick(e)} className={`button ${type}`}>
      {title}
    </button>
  );
};

export default Button;
